#include <iostream>
#include "thread_safe_queue.hpp"
#include "launch_thread.h"
#include <unistd.h>

const int NUM_ITERATIONS = 20000;
const int NUM_CONSUMERS = 5;

/*
    TODO: 
          1. zaimplementować wielowątkowo bezpieczną kolejkę ( interface w thread_safe_queue.hpp )
          2. uruchomić producenta i konsumentow w wątkach

*/

void producer(thread_safe_queue<int> &q, u_int num_iterations, u_int num_consumers)
{
    std::cout << "producer" << std::endl;

    for(int i=0; i<num_iterations; ++i)
    {
        q.push(i);
        sleep(1);
    }

    for(int i=0; i<num_consumers; ++i)
    {
        q.push(-1);
    }
}


void consumer(thread_safe_queue<int> &q, int id)
{
    while( true )
    {
        int value;
        q.pop(value);

        if( value == -1)
        {
            std::cout<< "quit" << std::endl;
            break;
        }

        std::cout << "#" << id << " rcv: " << value << std::endl;

        sleep(1);
    }
}


int main(int argc, char **argv) 
{
    thread_safe_queue<int> q;

    pthread_t tid_producer;
    pthread_t tid_consumer[NUM_CONSUMERS];

    launch_thread(&tid_producer, std::bind(producer, std::ref(q), NUM_ITERATIONS, NUM_CONSUMERS ) );

    for(int i=0; i<NUM_CONSUMERS; ++i)
    {
        launch_thread(&tid_consumer[i], std::bind(consumer, std::ref(q), i));
    }

    pthread_join(tid_producer, NULL);
    for(int i=0; i<NUM_CONSUMERS; ++i)
    {
        pthread_join(tid_consumer[i], NULL);
    }

    return 0;
}
