#pragma once

#include <pthread.h>
#include <queue>

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;

    pthread_cond_t cond;
    pthread_mutex_t mtx;

public:
    thread_safe_queue()
    {
        pthread_mutexattr_t mutexattr;
        pthread_mutexattr_init(&mutexattr);
        pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&mtx, &mutexattr);
        pthread_mutexattr_destroy(&mutexattr);

        pthread_cond_init(&cond, NULL);
    }

    ~thread_safe_queue()
    {
        pthread_mutex_destroy(&mtx);
        pthread_cond_destroy(&cond);
    }

    void pop(T& item)
    {
        pthread_mutex_lock(&mtx);

        while ( is_empty() )
        {
            pthread_cond_wait(&cond, &mtx);
        }

        item = q.front();
        q.pop();

        pthread_mutex_unlock(&mtx);
    }

    void push(T item)
    {
        pthread_mutex_lock(&mtx);

        q.push(item);
        pthread_cond_signal( &cond );

        pthread_mutex_unlock(&mtx);
    }

    bool is_empty()
    {
        pthread_mutex_lock(&mtx);

        bool val = q.empty();

        pthread_mutex_unlock(&mtx);

        return val;
    }
};

