#ifndef FUTURES_H
#define FUTURES_H
#include <iostream>
#include <functional>
#include <memory>

void *thread_routine( void * ptr_to_functor);
void launch_thread(pthread_t *id, std::function<void()> f);


template <typename T>
class ptask
{
    T& result;

    std::function<T()> task;

public:

    ptask( std::function<T()> t, T& r ) : result(r), task(t)
    {
    }

    void operator() ()
    {
        std::cout << "ptask - start" << std::endl;

        result = task();

        std::cout << "ptask - end" << std::endl;
    }
};


template <typename T>
class future
{
    pthread_t id;
    T result;

    ptask<T> task;

public:

    future( std::function<T()> t) : task( ptask<T>(t, result) )
    {
        launch_thread(&id, std::move(task) );  // move
    }

    ~future()
    {
        pthread_join(id, NULL);
    }

    T get()
    {
        pthread_join(id, NULL);
        return result;
    }

};


template <typename T>
future<T> async( std::function<T()> f )
{
    return future<T>(f);
}


#endif // FUTURES_H
