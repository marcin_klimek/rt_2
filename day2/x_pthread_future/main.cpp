#include <iostream>
#include <pthread.h>
#include "futures.h"
#include <memory>
#include <unistd.h>

using namespace std;

int answer()
{
    sleep(1);
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;

    future<int> res = async<int>(&answer );
    future<int> res2 = async<int>( []() -> int {return 6;} );



    cout << res.get() << endl;
    cout << res2.get() << endl;

    return 0;
}

