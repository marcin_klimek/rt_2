#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H
#include <string>
#include <iostream>
#include <functional>
#include <chrono>
#include <ctime>
#include "thread_safe_queue.h"
#include <pthread.h>
#include <unistd.h>
#include "launch_thread.h"

class Logger
{
public:
    Logger() {};
    void message(std::string msg)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::string time = std::ctime(&now);
        time.erase(time.find('\n', 0),1);
        sleep(1);
        std::cout << time;
        std::cout << " >> " << msg << std::endl;
    }
};

typedef std::function< void() > task_t;

class ActiveObject
{
    thread_safe_queue<task_t>
    pthread_t ao_worker;

    public:

    void worker()
    {

        while( true )
        {
            task_t f;

            std::cout << "waiting on queue" << std::endl;

            queue.pop(f);

            if (f)
            {
                f();
            }
            else
            {
                std::cout << "quit" << std::endl;
                break;
            }
        }
    }
}



class LoggerAsync
{
    ActiveObject ao;
public:
    LoggerAsync() {};
    void message(std::string msg)
    {
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::string time = std::ctime(&now);
        time.erase(time.find('\n', 0),1);
        ao.Send( [time, msg]() {
            sleep(1);
            std::cout << time;
            std::cout << " >> " << msg << std::endl;
        } );
    }

};

#endif // ACTIVE_OBJECT_H
