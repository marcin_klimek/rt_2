#include <iostream>
#include <pthread.h>
#include <functional>
#include <memory>
#include <unistd.h>
#include "threadpool.h"
#include "launch_thread.h"

using namespace std;

/*
   TODO:   zaimplementować wzorzec thread pool przy uzyciu thread_safe_queue i ew. launch_thread i bind
*/

void foo(int id)
{
    std::cout << "Foo " << id << std::endl;
}


class Bar
{
    int id;
public:

    Bar(int i) : id(i)
    {
        pthread_t id;
        launch_thread(&id, std::bind(&Bar::work, this) );
    }

    void operator() ()
    {
        std::cout << "Bar::operator " << id << std::endl;
    }

    void work()
    {
        std::cout << "Bar::work " << id << std::endl;
    }
};


int main()
{
    threadpool tp(2);

    tp.add_task( [](){cout << "Hello from first task" << endl; sleep(1);} );
    tp.add_task( [](){cout << "Hello from second task" << endl;} );
    tp.add_task( [](){cout << "Hello from first task" << endl;} );
    tp.add_task( [](){cout << "Hello from second task" << endl;} );
    tp.add_task( [](){cout << "Hello from first task" << endl;} );
    tp.add_task( [](){cout << "Hello from second task" << endl;} );
    tp.add_task( [](){cout << "Hello from first task" << endl;} );
    tp.add_task( [](){cout << "Hello from second task" << endl;} );
    tp.add_task( [](){cout << "Hello from first task" << endl;} );
    tp.add_task( [](){cout << "Hello from second task" << endl;} );


    Bar b(42);
    tp.add_task( std::bind(foo, 1));
    tp.add_task( std::bind(&Bar::work, b) );
    tp.add_task( std::bind(b));




    return 0;
}

