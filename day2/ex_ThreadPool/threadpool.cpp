#include "threadpool.h"
#include "launch_thread.h"

threadpool::threadpool(unsigned int ps)
    : pool_size(ps), threads(ps)

{
    for(int i=0; i<pool_size; ++i)
    {
        launch_thread(&threads[i], std::bind(&threadpool::worker, this) );
    }
}

threadpool::~threadpool()
{
   for(int i=0; i<pool_size; ++i)
   {
       queue.push(nullptr);
   }

   for(int i=0; i<pool_size; ++i)
   {
       pthread_join(threads[i], NULL);
   }
}

void threadpool::add_task(task_t f)
{
    queue.push( f );
}

void threadpool::worker()
{

    while( true )
    {
        task_t f;

        std::cout << "waiting on queue" << std::endl;

        queue.pop(f);

        if (f)
        {
            f();
        }
        else
        {
            std::cout << "quit" << std::endl;
            break;
        }
    }
}
