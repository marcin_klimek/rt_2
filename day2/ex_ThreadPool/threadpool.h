#pragma once
#include <iostream>
#include <pthread.h>
#include <functional>
#include "thread_safe_queue.hpp"
#include <pthread.h>

typedef std::function<void()> task_t;

class threadpool
{
    int        pool_size;
    std::vector<pthread_t> threads;

    thread_safe_queue<task_t> queue;

    void worker();

public:
    threadpool(unsigned int ps);
    ~threadpool();
    void add_task( task_t f);
};
