#include <pthread.h>
#include <iostream>
#include <sched.h>
#include <unistd.h>

void* worker(void*)
{
    std::cout << "worker" << std::endl;

    pthread_attr_t attr;
    sched_param sp;

    int policy;

    pthread_t tid = pthread_self();

    pthread_attr_init(&attr);

    pthread_getschedparam(tid, &policy, &sp);

    std::cout << "Policy ";

    switch( policy )
    {
    case SCHED_FIFO:
        std::cout << "FIFO" << std::endl;
        break;
    case SCHED_RR:
        std::cout << "RR" << std::endl;
        break;
    case SCHED_OTHER:
        std::cout << "OTHER" << std::endl;
        break;
    }

    std::cout << "prio " << sp.sched_priority << std::endl;

}

int main(int argc, char** argv)
{
    pthread_t id;
    pthread_attr_t attr;
    sched_param sp;

    int pmin;
    int pmax;

    int sched_policy;

    sched_policy = SCHED_FIFO;

    pmin = sched_get_priority_min(sched_policy);
    pmax = sched_get_priority_max(sched_policy);

    pthread_attr_init(&attr);

    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr, sched_policy);

    pthread_attr_getschedparam(&attr, &sp);
    sp.sched_priority = pmin;

    pthread_attr_setschedparam(&attr, &sp);

    pthread_create(&id, &attr, worker, NULL);

    pthread_join(id, NULL);
}
