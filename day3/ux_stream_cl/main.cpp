#include <iostream>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#define SV_SOCK_PATH "/tmp/ux_sock"
#define BUF_SIZE 1024


int server()
{
    int sfd;
    sockaddr_un addr;

    char rcv_buf[BUF_SIZE];

    unlink(SV_SOCK_PATH);


    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sfd == -1)
    {
        std::cout << "sock error" << std::endl;
        return 1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(addr.sun_path)-1);

    if ( bind(sfd, (sockaddr*)&addr, sizeof(addr) ) == -1)
    {
        std::cout << "bind error" << std::endl;
        return 1;
    }

    if ( listen(sfd, 1) == -1)
    {
        std::cout << "listen error" << std::endl;
        return 1;
    }

    while(true)
    {
        std::cout << "server, accept" << std::endl;
        int cfd = accept(sfd, NULL, NULL);

        int num_read = read(cfd, rcv_buf, sizeof(rcv_buf)-1);

        std::cout << "RECV " << num_read << " : "  << rcv_buf << std::endl;
        write(cfd, rcv_buf, num_read);

        close(cfd);
    }

}


int client()
{
    int sfd;
    sockaddr_un addr;

    const char* buf = "foobar";
    char rcv_buf[BUF_SIZE];


    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sfd == -1)
    {
        std::cout << "sock error" << std::endl;
        return 1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(addr.sun_path)-1);

    connect(sfd, (sockaddr*)&addr, sizeof(addr));

    write(sfd, buf, strlen(buf));

    memset(&rcv_buf, 0, sizeof(rcv_buf));
    read(sfd, rcv_buf, sizeof(rcv_buf)-1);

    std::cout << "RCV " << rcv_buf << std::endl;

    close(sfd);

    return 0;
}


int main(int argc, char** argv)
{
    if(argc > 1)
        client();
    else
        server();
}
