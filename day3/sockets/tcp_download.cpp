#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include "net/tcp_socket.h"
#include "thread/thread_safe_queue.h"
#include "thread/launch_thread.h"
#include "thread/threadpool.h"
#include <sys/fcntl.h>

constexpr int http_len = strlen("http://");

std::string send_msg = std::string("GET ")+url + " HTTP/1.1\r\nConnection: close\r\n\r\n";

//----------------
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>

string message = "A^B^C^D";
vector<string> tokens;
boost::split(tokens, message, boost::is_any_of("^"));
//----------------


std::string get_host(const std::string& url)
{
    std::string host = url.substr( http_len, url.length());
    return host.substr( 0, host.find("/"));
}

std::string get_file(const std::string& url)
{
    std::string file = url.substr( url.find_last_of("/")+1, url.length());
    if (file.length() == 0)
        file = get_host(url)+"_index";

    return file;
}
