#include "net/tcp_socket.h"
#include "thread/thread_safe_queue.h"
#include "thread/launch_thread.h"
#include <iostream>
#include <vector>
#include <pthread.h>
#include <list>
#include <algorithm>

std::list< net::tcp_socket::sockhandle > clients;

static const char send_msg[] = "Shut up and take my money!";
typedef std::list<net::tcp_socket::sockhandle>::iterator sock_iter_t;

sock_iter_t handle_client(sock_iter_t& it)
{
    auto handle = *it;

    std::cout << "client " <<  handle << std::endl;

    net::tcp_socket session_socket(handle);
    session_socket.send( send_msg, sizeof(send_msg) );

    if( session_socket.peerDisconnected())
        return clients.erase( it );;

    std::cout << "TCP server sucessfully sent data to the client." << std::endl;

    session_socket.disconnect();

    return clients.erase( it );
}

int main()
{
    fd_set readfds;

    try
    {
        net::tcp_socket server_socket;

        server_socket.bind( "127.0.0.1", 12345);
        server_socket.listen();

        std::cout << "server socket: " << server_socket.getfd() << std::endl;

        while( true )
        {
            FD_ZERO(&readfds);
            FD_SET( server_socket.getfd(), &readfds );

            int max_fd = server_socket.getfd();

            for (auto client : clients)
            {
                FD_SET (client, &readfds);
                max_fd = std::max<int>(client, max_fd);
            }

            select(max_fd+1, &readfds, NULL, NULL, NULL);

            if ( FD_ISSET(server_socket.getfd(), &readfds) )
            {
                std::cout << "new client" << std::endl;

                net::tcp_socket::sockhandle handle = server_socket.accept();
                if ( !handle )
                    return 1;

                clients.push_back( handle );
            }

            for( auto it=clients.begin(); it != clients.end(); )
            {
                if( FD_ISSET(*it, &readfds) )
                {
                    it = handle_client( it );
                }
                else
                    ++it;
            }

            //    handle_client( handle) );
        }
    }
    catch( const net::socket_exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}

