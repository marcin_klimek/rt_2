#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include "net/tcp_socket.h"
#include "thread/thread_safe_queue.h"
#include "thread/launch_thread.h"
#include "thread/threadpool.h"
#include <sys/fcntl.h>

constexpr int http_len = strlen("http://");

typedef thread_safe_queue<std::string> tsq_t;

void load(tsq_t& urls)
{
    std::ifstream list_file("list.txt");

    std::copy(std::istream_iterator<std::string>(list_file),
             std::istream_iterator<std::string>(),
             std::back_inserter(urls));
}

std::string get_host(const std::string& url)
{
    std::string host = url.substr( http_len, url.length());
    return host.substr( 0, host.find("/"));
}

std::string get_file(const std::string& url)
{
    std::string file = url.substr( url.find_last_of("/")+1, url.length());
    if (file.length() == 0)
        file = get_host(url)+"_index";

    return file;
}


class downloader
{
    tsq_t& urls;

public:

    downloader(tsq_t& q): urls(q)
    {
    }


    void get(const std::string& url)
    {
        std::string host = get_host(url);
        std::string filename = get_file(url);
        std::cout << host << " - " <<  filename << std::endl;

        std::ofstream file_out( std::string("get/" + filename));

        try
        {
            char recv_msg[1024];
            std::string send_msg = std::string("GET ") + url + " HTTP/1.1\r\nConnection: close\r\n\r\n";

            net::tcp_socket client_socket;
            client_socket.connect( host, 80);


            int bytes_sent = client_socket.send( send_msg.c_str(), send_msg.length());
            std::cout << "Sent data to server: " << bytes_sent  << std::endl;

            int bytes_recv;
            do
            {
                memset(recv_msg, 0, sizeof(recv_msg));
                bytes_recv = client_socket.receive( recv_msg, sizeof(recv_msg) );

                // we got a message!
                if(bytes_recv > 0)
                {
                    std::cout << "Received data from server: " << bytes_recv << std::endl;
                    file_out << recv_msg;
                }

            } while(bytes_recv>0);

            client_socket.disconnect();
        }
        catch( const net::socket_exception& e)
        {
            std::cerr << e.what() << std::endl;
        }
    }

    void operator() ()
    {
        while(!urls.is_empty())
        {
            std::string url;
            urls.pop(url);

            get(url);
        }
    }
};

int main()
{
    tsq_t urls;

    load(urls);

    threadpool tp(4);
    tp.add_task( downloader(urls) );
    tp.add_task( downloader(urls) );
    tp.add_task( downloader(urls) );
    tp.add_task( downloader(urls) );

    return 0;
}
