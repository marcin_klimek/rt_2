#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <queue>
#include <unistd.h>
#include <iostream>

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

pthread_t id;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

std::queue<int> q;

void* th_producent(void* arg)
{
    int errno;
    int i = 0;

    while( true )
    {
        pthread_mutex_lock( &mutex );

        q.push( i++ );

        std::cout << "Producent " << std::endl;

        pthread_mutex_unlock( &mutex );
        pthread_cond_broadcast( &cond );

        sleep(1);
    }

    return NULL;
}


void* th_konsument(void* arg)
{
    int errno;

    int idx = (int)(ssize_t)arg;

    while( true )
    {
        pthread_mutex_lock( &mutex );

        while ( q.empty() )
        {
            std::cout << "konsument - wait " << idx << std::endl;
            pthread_cond_wait( &cond, &mutex );
        }

        int value = q.front();
        q.pop();

        std::cout << "Konsument " << value << " " << idx << std::endl;

        pthread_mutex_unlock( &mutex );
    }


    return NULL;
}

//------------------------------------------------------------------------

int main(int argc, char* argv[])
{
    int errno;

    pthread_t id_prod;
    pthread_t id_cons;

    /* create */
    errno = pthread_create(&id_prod, NULL, th_producent, NULL);
    test_errno("pthread_create");

    for(int i=0; i<10; ++i)
    {
        errno = pthread_create(&id_cons, NULL, th_konsument, (void*)(ssize_t)i);
        pthread_detach( id_cons );
    }

    /* wait */
    pthread_join(id_prod, NULL);
    test_errno("pthread_join");

}
