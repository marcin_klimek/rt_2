#include <iostream>
#include <pthread.h>
#include <cxxabi.h>


using namespace std;

const long K = 20000;
const long N = 10;

long counter;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;


class lock_guard
{
    pthread_mutex_t &lock;

    public:

    lock_guard( pthread_mutex_t &l) : lock(l)
    {
        pthread_mutex_lock( &lock );
    }

    ~lock_guard()
    {
        pthread_mutex_unlock( &lock );
    }
}


void *worker(void *arg)
{
    for (int i = 0 ; i < K ; ++i)
    {
        lock_guard lk( lock );

        counter++;

        if ( counter == 1000)
        {
            cout << "counter == 1000" << endl;
            pthread_exit(NULL);
        }
        else
        {
            if ( counter % 42 == 0)
            {
                cout << "counter " << counter << endl;
                counter+=7;
                if ( counter % 197 == 0 )
                {
                    cout << "counter % 197" << endl;
                    return NULL;
                }
            }
        }
    }

    return NULL;
}

/*
TODO:  program ma się zakończyć, należy wyszukać problemowe miejsca
       zaproponować rozwiązanie oparte o C++
*/

int main()
{
    counter = 0;
    cout << "Starting, counter = " << counter << endl;
    pthread_t id[N];

    for (int i = 0 ; i < N ; ++i)
        pthread_create(&id[i], NULL, worker, NULL);

    for (int i = 0 ; i < N ; ++i)
        pthread_join(id[i], NULL);

    return 0;
}

