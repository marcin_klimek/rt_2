#include <iostream>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

const int NUM_THREADS=10;

struct thread_data
{
    int id;
    int value;
};


void* worker(void* targ)
{
    thread_data* arg = (thread_data*)targ;

    std::cout << "Thread worker " << arg->id << " " << arg->value << std::endl;

    arg->value = 42;

    return (void*)arg;
}


int main()
{
    thread_data data[NUM_THREADS];
    pthread_t tid[NUM_THREADS];

    for(int i=0; i<NUM_THREADS; ++i)
    {
        data[i].id = i;
        data[i].value = 0;
        pthread_create(&tid[i], NULL, worker, (void*)&data[i]);
    }

    for(int i=0; i<NUM_THREADS; ++i)
    {
        thread_data* data_ret;
        pthread_join(tid[i], (void**)&data_ret);
        std::cout << data_ret->id << " " << data_ret->value << std::endl;

    }

    for(int i=0; i<NUM_THREADS; ++i)
    {
        std::cout << data[i].value << std::endl;
    }

    //std::cout << "Ret value " << value << std::endl;

    return 0;
}
