#include <iostream>
#include <functional>
#include <memory>

void* thread_routine(void* ptr)
{
    std::unique_ptr< std::function< void() > > task( static_cast<std::function<void()>* >(ptr));
    (*task)();

    return NULL;
}


void launch_thread(pthread_t *id, std::function< void() > f)
{
    std::function< void() >* f_ptr = new std::function< void() >(f);

    int err = pthread_create( id, NULL, thread_routine, f_ptr );

    if ( err != 0 )
    {
        delete f_ptr;
        throw std::exception();
    }
}



void run(int x, int &ret)
{
    std::cout << "from thread " << x << std::endl;

    ret = 42;
}

class Foo
{
    int x_;
public:

    Foo(int x) : x_(x)
    {
    }

    void operator() ()
    {
        std::cout << "from foo" << std::endl;
    }
};


int main()
{
    int value = 10;
    pthread_t id;

    launch_thread(&id, std::bind(run, 10, std::ref(value) ));
    pthread_join(id, NULL);
    std::cout << value << std::endl;

    pthread_t id2;

    Foo f(10);
    launch_thread(&id2, f);
    
    pthread_join(id2, NULL);

    pthread_t id3;

    auto func = []() { std::cout << "from lambda" <<std::endl; };

    launch_thread(&id3, func );
    pthread_join(id3, NULL);

    return 0;
}

