#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#define NUM_THREADS 8

char *messages[NUM_THREADS];

struct thread_data
{
    int  taskid;
    int  sum;
    char *message;
};

struct thread_data thread_data_array[NUM_THREADS];

void *worker(void *arg)
{
    int idx = *(int*)arg;
    int taskid, sum;
    char *hello_msg;
    struct thread_data *my_data = &thread_data_array[idx];
    
    sleep(1);

    taskid = my_data->taskid;
    sum = my_data->sum;
    hello_msg = my_data->message;
    printf("Thread %d: %s  Sum=%d\n", taskid, hello_msg, sum);
    
    pthread_exit(NULL);
}

/*
  TODO: dlaczego czasem wynik workera jest nieprawidlowy? 
        prosze naprawic dzialanie tego programu
*/

int main(int argc, char *argv[])
{
    pthread_t threads[NUM_THREADS];
    int *taskids[NUM_THREADS];
    int rc, sum;

    sum=0;
    messages[0] = "1 A";
    messages[1] = "2 B";
    messages[2] = "3 C";
    messages[3] = "4 D";
    messages[4] = "5 E";
    messages[5] = "6 F";
    messages[6] = "7 G";
    messages[7] = "8 H";

    for(int t=0; t<NUM_THREADS; t++)
    {
        sum = sum + t;
        thread_data_array[t].taskid = t;
        thread_data_array[t].sum = sum;
        thread_data_array[t].message = messages[t];
        printf("Creating thread %d\n", t);

        rc = pthread_create(&threads[t], NULL, worker, (void *)&t);
        if (rc)
        {
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }

    pthread_exit(NULL);
}
