#pragma once

#include <pthread.h>
#include <queue>

template<typename T>
class thread_safe_queue
{

public:
    thread_safe_queue()
    {
    }

    void pop(T& item)
    {
    }

    void push(T item)
    {
    }

    bool is_empty()
    {
        return true;
    }
};