long seq_sum(long* begin, long* end)
{
    long sum=0;
    for(; begin < end; ++begin)
        sum += *begin;

    return sum;
}
